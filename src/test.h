#include <stdio.h>
#include <unistd.h>

#include "mem.h"

void test_malloc(void* heap);

void test_free_one_of_two_allocated(void* heap);

void test_free_two_of_several_allocated(void* heap);

void test_heap_growth (void* heap);

void test_non_continous_heap_growth (void* heap);