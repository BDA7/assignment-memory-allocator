#include "test.h"

#include <stdio.h>
#include <unistd.h>

#include "mem.h"

void test_malloc(void* heap);
void test_free_one_of_two_allocated(void* heap);
void test_free_two_of_several_allocated(void* heap);
void test_heap_growth (void* heap);
void test_non_continous_heap_growth (void* heap);

void test_malloc(void* heap){
	printf("Test normal malloc:\n");
	printf("\t expected: one block with 25 bytes\n");
	void* allocated = _malloc(25);
	debug_heap(stdout, heap); 
	printf("\nCleanup:\n");
	_free(allocated);
	debug_heap(stdout,heap);
	printf("------------------------------------------\n\n");
}

void test_free_one_of_two_allocated(void* heap){
	printf("Test freeing one of two allocated blocks:\n");
	printf("\t expected: one free block with 69 bytes, 34 occupied and the remainder is free\n");
	void* allocated1 = _malloc(69);
	void* allocated2 = _malloc(34);
	_free(allocated1);
	debug_heap(stdout, heap);
	printf("\nCleanup:\n");
	_free(allocated2);
	debug_heap(stdout, heap);
	printf("------------------------------------------\n\n");
}

void test_free_two_of_several_allocated(void* heap){
	printf("Test freeing two of five allocated blocks:\n");
	printf("\t expected: 69-occupied 34-occupied 1582-free(comb of 3rd and 4th + block_header) 420-occupied\n");
	void* allocated1 = _malloc(69);
	void* allocated2 = _malloc(34);
	void* allocated3 = _malloc(228);
	void* allocated4 = _malloc(1337);
	void* allocated5 = _malloc(420);
	_free(allocated4);
	_free(allocated3);
	debug_heap(stdout, heap);
	printf("\nCleanup:\n");
	_free(allocated5);
	_free(allocated2);
	_free(allocated1);
	debug_heap(stdout, heap);
	printf("------------------------------------------\n\n");
}

void test_heap_growth (void* heap) {
    printf ("Test heap growth:\n");
	printf ("\t expected: heap over10000\n");
    void* a = _malloc(69000);
    debug_heap(stdout, heap);
    _free(a);
    printf("------------------------------------------\n");
}

void test_non_continous_heap_growth (void* heap) { // 5 test :)
    printf ("Test heap growth in non-continuous manner\n");
	printf ("\texpected: empty block of initial heap, occupied block of twice as much, some residue\n");
	size_t heap_sz = heap_size(heap);
    printf("\nHeap is %zu bytes\n", heap_sz);
    debug_heap(stdout, heap);

	void* heap_end = (void*)((uint8_t*)HEAP_START + heap_sz);
	void* region = map_region_precise(heap_end, 5000);
	printf("Allocated new region at the heap end: %p\n", region);
    debug_heap(stdout, heap);

    void* a = _malloc(heap_sz*2);
    debug_heap(stdout, heap);

    _free(a);
    debug_heap(stdout, heap);

    printf("\n");
}