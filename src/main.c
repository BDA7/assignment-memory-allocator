#include <stdio.h>
#include <unistd.h>
#include "mem.h"

#include "test.h"

int main(){
	printf("Init heap");
	void* heap = heap_init(10000);
	debug_heap(stdout, heap);

	test_malloc(heap);
	test_free_one_of_two_allocated(heap);
	test_free_two_of_several_allocated(heap);
	test_heap_growth(heap);
	test_non_continous_heap_growth(heap);
}